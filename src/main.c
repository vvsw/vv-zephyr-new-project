
/*
 *  * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *   *
 *    * SPDX-License-Identifier: Apache-2.0
 *     * verivina vvsw 2020
 *      */

#include <zephyr.h>
#include <sys/printk.h>

void main(void)
{
	        printk("Make sure to sleep tight tonight! ;) %s\n", CONFIG_BOARD);
}
